package cs.mad.flashcards.entities
import androidx.room.*


@Entity
data class Flashcard(
    val question: String,
    val answer: String,
    @PrimaryKey val id: Int) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i",i))
            }
            return cards
        }
    }
}
@Dao
interface FlashcardDaoIf {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(card: Flashcard)

    @Insert
    suspend fun insertAll(cards: List<Flashcard>)

    @Update
    suspend fun update(card: Flashcard)

    @Delete
    suspend fun delete(card: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()

}